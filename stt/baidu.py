from aip import AipSpeech
from loguru import logger

APP_ID = '33570571'
API_KEY = '46CnWyv6al2GGYCakATfmYnF'
SECRET_KEY = 'hym5w8yKykty5I2NY8FffpGxeCA73o4G'

"""
dev_pid 参数列表
dev_pid	语言	模型	是否有标点	备注
1537	普通话(纯中文识别)	语音近场识别模型	有标点	支持自定义词库
1737	英语	英语模型	无标点	不支持自定义词库
1637	粤语	粤语模型	有标点	不支持自定义词库
1837	四川话	四川话模型	有标点	不支持自定义词库
1936	普通话远场	远场模型	有标点	不支持
"""

def stt(speech: bytes,format='pcm',rate=16000,**options):
    client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
    logger.info("start")
    result = client.asr(speech, format, rate, options)
    logger.debug(result)
    if not result["err_no"]:
        return result["result"][0]
    else:
        logger.warning(f"error {result}")

if __name__ == '__main__':
    from utils import get_file_content
    options = {
        'dev_pid': 1537,
    }
    # options = None
    result = stt(get_file_content('../resource/16k-0.pcm'),options = options)
    logger.info(result)