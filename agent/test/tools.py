import os

os.environ["OPENAI_API_KEY"] = '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'
os.environ["SERPAPI_API_KEY"] = 'b350b31ed932aefbaada8f39843f1ef20bc69f9b28948fc51d0edcc6aa9fe4e0'
os.environ["OPENWEATHERMAP_API_KEY"] = "5258a1d65a21059130a70624dcf53dbc"

OPENAI_API_BASE = 'https://proxy.aitoy.fun/v1'

from langchain.agents import load_tools
from langchain.agents import initialize_agent, Tool
from langchain.llms import OpenAI
from langchain.agents import AgentType
from langchain.utilities import OpenWeatherMapAPIWrapper

# 加载 OpenAI 模型
llm = OpenAI(temperature=0,max_tokens=2048,api_base = OPENAI_API_BASE) 
weather = OpenWeatherMapAPIWrapper()
weather_tools = Tool(name = "weather-api",
                       func=weather.run,
                       description = "A wrapper around OpenWeatherMap API. Useful for fetching current weather information for a specified location. Input should be a location string in english (e.g. London,GB)."
                    )
tools=load_tools(["serpapi","python_repl","requests_all"]) + [weather_tools]
# 工具加载后都需要初始化，verbose 参数为 True，会打印全部的执行详情
agent = initialize_agent(tools, llm, agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION, verbose=True)
# 运行 agent
question = "介绍你自己"
print("输入：",question)
agent.run(question)