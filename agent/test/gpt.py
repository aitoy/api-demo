import os

os.environ["OPENAI_API_KEY"] = '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'
os.environ["SERPAPI_API_KEY"] = 'b350b31ed932aefbaada8f39843f1ef20bc69f9b28948fc51d0edcc6aa9fe4e0'
OPENAI_API_BASE = 'https://proxy.aitoy.fun/v1'


# OpenAI text-davinci-003
from langchain.llms import OpenAI
llm = OpenAI(model_name="text-davinci-003",max_tokens=1024,api_base = OPENAI_API_BASE)
llm("怎么评价人工智能")


from langchain.chat_models import ChatOpenAI
from langchain import PromptTemplate, LLMChain
from langchain.prompts.chat import (
    ChatPromptTemplate,
    SystemMessagePromptTemplate,
    AIMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.schema import (
    AIMessage,
    HumanMessage,
    SystemMessage
)

# gpt-3.5-turbo
chat = ChatOpenAI(model_name="gpt-3.5-turbo", api_base=OPENAI_API_BASE)
chat([HumanMessage(content="你好")])

# streaming
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
chat = ChatOpenAI(streaming=True, callbacks=[StreamingStdOutCallbackHandler()], temperature=0,api_base=OPENAI_API_BASE)
resp = chat([HumanMessage(content="Write me a song about sparkling water.")])

