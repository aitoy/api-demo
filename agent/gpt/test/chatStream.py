import openai
from loguru import logger
import re
import time

# 设置 OpenAI 的 API 密钥
openai.api_key = "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"

# 设置代理服务器
# openai.proxy = "https://proxy.aitoy.fun/v1/chat"
openai.api_base = "https://api.aitoy.fun/v1"
# openai.api_base = "https://chatgpt-proxy.klec.top/proxy-sse/v1"

# create a chat completion
logger.debug('start')
received = False
t0 = time.time()
buffer = ''
# 用于匹配标点符号的正则表达式
punctuation_pattern = r'[;。？，]'
response  = openai.ChatCompletion.create(
    model="gpt-3.5-turbo-0613",
    messages=[ 
            #   {"role": "system", "content": "speak in english"},
              {"role": "user", "content": "从1数到10"}],
    stream=True
)

for chunk in response:
    # logger.debug(f"received {chunk} ,用时{time.time()-t0}")
    if "content" in chunk['choices'][0]['delta'].keys():
        buffer += chunk['choices'][0]['delta']['content']
        logger.debug(f"buffer:{buffer}")
        match = re.search(punctuation_pattern, buffer[::-1])
        # logger.debug(f"match:{match},buffer:{buffer}")
        if match:
            # 如果找到标点符号，将文本分为两部分
            index = len(buffer) - match.end() + len(match[0]) # 注意这里
            paragraph = buffer[:index].strip()
            # logger.debug(f"paragraph:{paragraph}")
            buffer = buffer[index:]
            # 输出分段结果
            logger.debug(f"response:{paragraph}")
            # await websocket.send_text(paragraph)    

# for chunk in response:
#     # logger.debug(chunk)
#     if received == False:
#         logger.debug(f'received {len(chunk)} ,用时{time.time()-t0}')
#         received = True
    
logger.debug(f'done:{time.time() - t0}')
    # logger.debug(chunk["choices"]["finish_reason"])
    # if(chunk["choices"]["finish_reason"] != "stop"):
    #     logger.info(chunk.choices[0].message.content)
    # logger.info(chunk.choices[0].message.content)