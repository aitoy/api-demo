import openai
from loguru import logger
# 设置 OpenAI 的 API 密钥
openai.api_key = "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"

# 设置代理服务器
# openai.api_base = "https://proxy.aitoy.fun/v1"
openai.api_base = "https://chatgpt-proxy.klec.top/proxy/v1"

# create a chat completion
logger.debug('start')
chat_completion = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[{"role": "user", "content": "讲一个笑话"}]
)
logger.debug(chat_completion)
# print the chat completion
logger.info(chat_completion.choices[0].message.content)