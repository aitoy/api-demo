FROM python:3.11

COPY ./requirements.txt .
RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple --no-cache-dir

WORKDIR /app
COPY . .

# 启动 FastAPI 应用
ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0"]