from fastapi import APIRouter,WebSocket,WebSocketDisconnect
from loguru import logger
from revChatGPT.V3 import Chatbot
from aip import AipSpeech
import openai
import os,asyncio,time
import re 

router = APIRouter(prefix="/v1")

api_key = os.environ.get("api_key")
system_prompt = os.environ.get("system_prompt")
chatbot = Chatbot(
    api_key=api_key,
    system_prompt = system_prompt
)

APP_ID = '33570571'
API_KEY = '46CnWyv6al2GGYCakATfmYnF'
SECRET_KEY = 'hym5w8yKykty5I2NY8FffpGxeCA73o4G'

client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# - 根路径: / ,接收任意数据，返回文本'已收到'
@router.websocket("/")
async def audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            msg = await websocket.receive_bytes()
            logger.debug(f"Received message: {msg}")
            # await websocket.on_receive(data)
            await websocket.send_text('已收到')
    except WebSocketDisconnect:
        await websocket.close()

# - /text2text,接收文本(utf-8),返回chatgpt回复的文本
@router.websocket("/text2text")
async def text2text(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            text = data.decode('utf-8')
            logger.debug(f"Received msg {text}")
            try:
                response = chatbot.ask( text)
                logger.debug(f'gpt response: {response}')
                await websocket.send_text(response)
            except Exception as e:
                logger.error(e)
                await websocket.send_text(str(e))
    except WebSocketDisconnect:
        await websocket.close()
         
# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.websocket("/audio2audio")
async def audio2audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            logger.debug(f"百度语音识别: {result}")
            response = chatbot.ask(result['result'][0])
            logger.debug(f'gpt response: {response}')
            audio_data  = client.synthesis(response, 'zh', 4, {'vol': 5,})
            logger.debug(f"百度api合成数据长度: {len(audio_data)}")
            await websocket.send_bytes(audio_data)
    except WebSocketDisconnect:
        await websocket.close()
        
     
# - /audio2gpt,接收文本数据utf-8编码，返回gpt回复的文本
@router.websocket("/audio2text")
async def audio2gpt(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
    
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            
            logger.debug(f"百度语音识别: {result}")
            
            with open(f"resource/audio/{result['corpus_no']}.pcm", 'wb') as f:
                f.write(data)
                
            response = await chatbot.ask_async(result['result'][0])
            logger.debug(f'gpt response: {response}')
            await websocket.send_text(response)
    except WebSocketDisconnect:
        await websocket.close()
