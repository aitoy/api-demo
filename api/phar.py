# 品恒开发板接口

from fastapi import APIRouter,WebSocket
from loguru import logger
from revChatGPT.V3 import Chatbot 
import os

api_key = os.environ.get("api_key")
system_prompt = os.environ.get("system_prompt")
chatbot = Chatbot(
    api_key=api_key,
    system_prompt = system_prompt
)

router = APIRouter()

# 品恒开发板接口
@router.websocket("//PHASR0000c049efbdb7e5")
async def audio(websocket: WebSocket):
    # logger.debug('ws connect')
    # 打开WebSocket连接
    await websocket.accept()
    # logger.debug('ws accept')

    while True:
        # 从WebSocket接收音频数据
        msg = await websocket.receive()
        logger.debug(f"Received message: {msg}")
        if 'bytes' in msg.keys():
            response = chatbot.ask( msg['bytes'].decode('gbk'))
            await websocket.send_text(response.encode('gbk'))
        elif "text" in msg.keys():
            response = chatbot.ask( msg['text'])
            await websocket.send_text(response)
        else:
            logger.warning("Unknown message type.")
            break
        

