
from fastapi import APIRouter, File, UploadFile
from loguru import logger
import openai
import os,asyncio,time
import requests

router = APIRouter(prefix="/whisper")

# 设置 API 地址和请求头
OPENAI_API_KEY ="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"
API_URL = "https://api.openai.com/v1/audio/transcriptions"

headers = {"Authorization": f"Bearer {OPENAI_API_KEY}"}

@router.post("/")
async def upload_audio(file: UploadFile):
    # 在这里处理上传的音频文件
    logger.debug({"filename":file.filename,"content_type":file.content_type})
    files = {"file": (file.filename, file.file,"application/octet-stream")}
    data = {"model": "whisper-1","prompt":"以下是普通话的句子","language":"zh"}

    response = requests.post(API_URL, headers=headers, files=files, data=data,stream=True)
    logger.debug(response.json())

    return response.json()

