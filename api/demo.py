from fastapi import APIRouter, File, UploadFile,WebSocket,WebSocketDisconnect
from fastapi.responses import StreamingResponse
from loguru import logger

router = APIRouter(prefix="/demo")

@router.get("/items/{item}")
def get_item(item):
    return {"Item": item}

# 定义 /items 路径的 POST 请求处理程序
@router.post("/items")
def post_item(item: str):
    return {"Item": item}

@router.post("/upload")
async def upload_audio(file: UploadFile):
    # 在这里处理上传的音频文件
    # 可以进行音频处理、分析、存储等操作
    # 这里仅仅返回一个响应示例
    return {"filename": file.filename, "content_type": file.content_type}

@router.get("/audio")
async def get_audio():
    # 读取示例MP3文件
    mp3_path = "./resource/启动.mp3"
    return StreamingResponse(open(mp3_path, "rb"), media_type="audio/mpeg")

@router.get("/ws")
async def get_audio():
    data = bytes([0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39]) *100 * 1024
    logger.debug(f"Send message: {len(data)}")
    return data

# 可接收任意格式的数据
@router.websocket("/ws")
async def audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            msg = await websocket.receive()
            logger.debug(f"Received message: {len(msg)}")
            if(msg['type'] == 'websocket.disconnect'):
                break
            # await websocket.on_receive(data)
            data = bytes([0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39]) * 1024 *100
            logger.debug(f"Send message: {len(data)}")
            await websocket.send_bytes(data)
            logger.debug(f"Send message: {len(data)} done")
    except WebSocketDisconnect:
        await websocket.close()