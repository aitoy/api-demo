from fastapi import APIRouter,WebSocket,WebSocketDisconnect
from loguru import logger
from aip import AipSpeech
import openai
import os,asyncio,time
import re 

router = APIRouter(prefix="/v2")

openai.api_base = "https://proxy.aitoy.fun/v1"
openai.api_key = os.environ.get("api_key")

APP_ID = '33570571'
API_KEY = '46CnWyv6al2GGYCakATfmYnF'
SECRET_KEY = 'hym5w8yKykty5I2NY8FffpGxeCA73o4G'

client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.websocket("/audio")
async def audio2audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            logger.debug(f"百度语音识别: {result}")
            t0 = time.time()
            try:
                response  = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=[{"role": "user", "content": result['result'][0]}],
                    stream=True
                )
                buffer = ''
                # 用于匹配标点符号的正则表达式
                punctuation_pattern = r'[,.!?;。，？！]'
                for chunk in response:
                    #  logger.debug(f"received {chunk} ,用时{time.time()-t0}")
                    if "content" in chunk['choices'][0]['delta'].keys():
                        buffer += chunk['choices'][0]['delta']['content']
                        match = re.search(punctuation_pattern, buffer[::-1])
                        # logger.debug(f"match:{match},buffer:{buffer}")
                        if match:
                            # 如果找到标点符号，将文本分为两部分
                            index = len(buffer) - match.end() + len(match[0]) # 注意这里
                            paragraph = buffer[:index].strip()
                            buffer = buffer[index:]
                            # 输出分段结果
                            logger.debug(f"response:{paragraph}")
                            await websocket.send_text(paragraph)    
                            audio_data  = client.synthesis(paragraph, 'zh', 4, {'vol': 5,})
                            logger.debug(f"百度api合成数据长度: {len(audio_data)}")
                            await websocket.send_bytes(audio_data)
                logger.debug(f"处理完毕,用时{time.time()-t0}")
            except Exception as e:
                logger.error(e)
                await websocket.send_text('gpt调用太频繁啦,请稍后再试')
    except WebSocketDisconnect:
        logger.warning("Disconnected")
        # await websocket.close()
        
        
# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.websocket("/audio2audio")
async def audio2audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            logger.debug(f"百度语音识别: {result}")
            t0 = time.time()
            try:
                response  = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=[{"role": "user", "content": result['result'][0]}],
                    stream=True
                )
                buffer = ''
                # 用于匹配标点符号的正则表达式
                punctuation_pattern = r'[,.!?;。，？！]'
                for chunk in response:
                    #  logger.debug(f"received {chunk} ,用时{time.time()-t0}")
                    if "content" in chunk['choices'][0]['delta'].keys():
                        buffer += chunk['choices'][0]['delta']['content']
                        match = re.search(punctuation_pattern, buffer[::-1])
                        # logger.debug(f"match:{match},buffer:{buffer}")
                        if match:
                            # 如果找到标点符号，将文本分为两部分
                            index = len(buffer) - match.end() + len(match[0]) # 注意这里
                            paragraph = buffer[:index].strip()
                            buffer = buffer[index:]
                            # 输出分段结果
                            logger.debug(f"response:{paragraph}")
                            # await websocket.send_text(paragraph)    
                            audio_data  = client.synthesis(paragraph, 'zh', 4, {'vol': 5,})
                            logger.debug(f"百度api合成数据长度: {len(audio_data)}")
                            await websocket.send_bytes(audio_data)
                            
                logger.debug(f"处理完毕,用时{time.time()-t0}")
            except Exception as e:
                logger.error(e)
                with open(f"resource/gpt_error.pcm", 'rb') as f:
                    data = f.read()
                    await websocket.send_bytes(data)
                        
    except WebSocketDisconnect:
        logger.warning("Disconnected")
        # await websocket.close()
        
# - /audio2gpt,接收文本数据utf-8编码，返回gpt回复的文本
@router.websocket("/audio2text")
async def audio2text(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
    
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            
            logger.debug(f"百度语音识别: {result}")
            
            t0 = time.time()
            try:
                response  = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=[{"role": "user", "content": result['result'][0]}],
                    stream=True
                )
                buffer = ''
                # 用于匹配标点符号的正则表达式
                punctuation_pattern = r'[,.!?;。，？！]'
                for chunk in response:
                    #  logger.debug(f"received {chunk} ,用时{time.time()-t0}")
                    if "content" in chunk['choices'][0]['delta'].keys():
                        buffer += chunk['choices'][0]['delta']['content']
                        match = re.search(punctuation_pattern, buffer[::-1])
                        # logger.debug(f"match:{match},buffer:{buffer}")
                        if match:
                            # 如果找到标点符号，将文本分为两部分
                            index = len(buffer) - match.end() + len(match[0]) # 注意这里
                            paragraph = buffer[:index].strip()
                            buffer = buffer[index:]
                            # 输出分段结果
                            logger.debug(f"response:{paragraph}")
                            await websocket.send_text(paragraph)    
                logger.debug(f"处理完毕,用时{time.time()-t0}")
            except Exception as e:
                logger.error(e)
                await websocket.send_text('gpt调用太频繁啦,请稍后再试')

    except WebSocketDisconnect:
        logger.warning("Disconnected")
        # await websocket.close()
        
# - /test,测试音频发送接收
@router.websocket("/test")
async def test(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive()
            logger.debug(f"Received data : {data}")
            if(data['type'] == 'websocket.disconnect'):
                break
            with open(f"resource/16k-0.pcm", 'rb') as f:
                    data = f.read()
                    logger.debug(f"send data size: {len(data)}")
                    await websocket.send_bytes(data)
    except WebSocketDisconnect:
        logger.warning("Disconnected")
        # await websocket.close()


       
# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.websocket("/pcm2mp3")
async def pcm2mp3(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
            })
            logger.debug(f"百度语音识别: {result}")
            t0 = time.time()
            try:
                response  = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo",
                    messages=[
                        {"role": "system", "content": "控制每句话的长度在 10个字以内,用分号 ';' 作为短句结尾"},
                        {"role": "user", "content": result['result'][0]}
                        ],
                    stream=True
                )
                buffer = ''
                # 用于匹配标点符号的正则表达式
                punctuation_pattern = r'[;。？，；]'
                for chunk in response:
                    # logger.debug(f"received {chunk} ,用时{time.time()-t0}")
                    if "content" in chunk['choices'][0]['delta'].keys():
                        buffer += chunk['choices'][0]['delta']['content']
                        match = re.search(punctuation_pattern, buffer[::-1])
                        # logger.debug(f"match:{match},buffer:{buffer}")
                        if match:
                            # 如果找到标点符号，将文本分为两部分
                            index = len(buffer) - match.end() + len(match[0]) # 注意这里
                            paragraph = buffer[:index].strip()
                            buffer = buffer[index:]
                            # 输出分段结果
                            logger.debug(f"response:{paragraph}")
                            # await websocket.send_text(paragraph)    
                            audio_data  = client.synthesis(paragraph, 'zh', 3, {'vol': 5,})
                            logger.debug(f"百度api合成数据长度: {len(audio_data)}")
                            await websocket.send_bytes(audio_data)
                            
                logger.debug(f"处理完毕,用时{time.time()-t0}")
            except Exception as e:
                logger.error(e)
                with open(f"resource/gpt_error.pcm", 'rb') as f:
                    data = f.read()
                    await websocket.send_bytes(data)
                        
    except WebSocketDisconnect:
        logger.warning("Disconnected")
        # await websocket.close()
        