from fastapi import APIRouter,WebSocket,WebSocketDisconnect,File
from fastapi.responses import StreamingResponse
from aip import AipSpeech
from loguru import logger
from io import BytesIO
from pydantic import BaseModel

router = APIRouter(prefix='/baidu')

APP_ID = '33570571'
API_KEY = '46CnWyv6al2GGYCakATfmYnF'
SECRET_KEY = 'hym5w8yKykty5I2NY8FffpGxeCA73o4G'

client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# - /audio2text,接收pcm音频数据,返回百度api识别的文本
@router.post("/audio2text")
async def post_audio2text(data: bytes = File()):
    logger.debug(f"Received data size: {len(data)}")
    result = client.asr(data, 'pcm', 16000, {
        'dev_pid': 1537,
    })
    logger.debug(f"百度api识别{result}")
    return result
        
# - /audio2text,接收pcm音频数据,返回百度api识别的文本
@router.websocket("/audio2text")
async def ws_audio2text(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            logger.debug(f"Received data size: {len(data)}")
            result = client.asr(data, 'pcm', 16000, {
                'dev_pid': 1537,
                'aue': 4
            })
            logger.debug(f"百度api识别{result}")
            await websocket.send_json(result)
    except WebSocketDisconnect:
        await websocket.close()
           
# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.get("/text2audio")
async def get_text2audio(text: str = "欢迎使用乐鑫音频平台，想了解更多方案信息请联系我们"):
    logger.debug(f"Received msg {text}")
    audio_data  = client.synthesis(text, 'zh', 4, {'vol': 5,})
    logger.debug(f"百度api合成数据长度: {len(audio_data)}")
     # 返回StreamingResponse对象，将BytesIO流对象作为响应
    return StreamingResponse(BytesIO(audio_data), media_type="audio/mp3")


# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.post("/text2audio")
async def post_text2audio(text: str = "欢迎使用乐鑫音频平台，想了解更多方案信息请联系我们"):
    logger.debug(f"Received msg {text}")
    audio_data  = client.synthesis(text, 'zh', 4, {'vol': 5,})
    logger.debug(f"百度api合成数据长度: {len(audio_data)}")
     # 返回StreamingResponse对象，将BytesIO流对象作为响应
    return StreamingResponse(BytesIO(audio_data), media_type="audio/mp3")


class Text(BaseModel):
    text: str

@router.post("/text2audio2")
async def post_text2audio2(text: str):
    logger.debug(f"Received msg {text}")
    audio_data  = client.synthesis(text, 'zh', 4, {'vol': 5,})
    logger.debug(f"百度api合成数据长度: {len(audio_data)}")
    # 返回StreamingResponse对象，将BytesIO流对象作为响应
    # 将音频数据流式输出
    return StreamingResponse(BytesIO(audio_data), media_type="audio/mp3")

# - /text2audio,接收文本数据utf-8编码，返回百度api合成的pcm音频数据
@router.websocket("/text2audio")
async def ws_text2audio(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            text = await websocket.receive_text()
            # text = data.decode('utf-8')
            logger.debug(f"Received msg {text}")
            audio_data  = client.synthesis(text, 'zh', 1, {'vol': 5,'aue':3})
            if not isinstance(audio_data, dict):
                logger.debug(f"百度api合成数据长度: {len(audio_data)}")
                await websocket.send_bytes(audio_data)
                result = client.asr(audio_data, 'pcm', 16000, {
                    'dev_pid': 1537,
                })
                logger.debug(f"百度api识别{result}")
            else:
                logger.debug(f"百度api合成{audio_data}")
    except WebSocketDisconnect:
        logger.warning('disconnected')