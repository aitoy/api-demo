from fastapi import APIRouter,WebSocket,WebSocketDisconnect
from loguru import logger
from revChatGPT.V3 import Chatbot
from aip import AipSpeech
import openai
import os,asyncio,time
import re 

router = APIRouter(prefix="/gpt")

api_key = os.environ.get("api_key")
system_prompt = os.environ.get("system_prompt")
chatbot = Chatbot(
    api_key=api_key,
    system_prompt = system_prompt
)

# 定义 /items 路径的 POST 请求处理程序
@router.post("/")
def post_item(msg: str):
    return {"Item": msg}

# - /text2text,接收文本(utf-8),返回chatgpt回复的文本
@router.websocket("/demo")
async def chat(websocket: WebSocket):
    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_bytes()
            text = data.decode('utf-8')
            logger.debug(f"Received msg {text}")
            try:
                response = chatbot.ask( text)
                logger.debug(f'gpt response: {response}')
                await websocket.send_text(response)
            except Exception as e:
                logger.error(e)
                await websocket.send_text(str(e))
    except WebSocketDisconnect:
        await websocket.close()
