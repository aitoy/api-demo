import requests
import json
from loguru import logger
# 设置请求头和请求体
headers = {
    "Authorization": "Bearer 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",
    "Content-Type": "application/json"
}

# url = 'https://openai.wndbac.cn/v1/chat/completions'
# url = "https://api.aitoy.fun/v1/chat/completions"
url = "https://api.openai.com/v1/chat/completions"

def chat(msg = '从1数到100',stream = True):
    data = {
        "model": "gpt-3.5-turbo",
        "messages": [{"role": "user", "content": f"{msg}"}],
        "stream": stream,
    }

    # 发送 POST 请求
    logger.debug(f"start: {msg}")
    responses = requests.post(url, headers=headers, json=data)
    
    for response in responses:
        logger.debug(response)
    # logger.debug("end")
    # # 解析响应数据
    # if response.status_code == 200:
    #     response_data = json.loads(response.text)
    #     answer = response_data["choices"][0]["message"]
    #     logger.info(answer)
    # else:
    #     logger.warning("请求失败，状态码为：", response.status_code)

if __name__ == '__main__':
    chat('从1数到100')