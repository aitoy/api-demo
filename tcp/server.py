from twisted.internet import reactor, protocol
from loguru import logger
class MyServer(protocol.Protocol):
    def dataReceived(self, data):
        # 接收客户端发送的数据
        logger.debug("Data received: {}".format(data))
        # 发送数据到客户端
        data = b'0123456789' * 1024 * 100
        logger.debug('send start')
        self.transport.write(data)
        logger.debug('send done')

# 创建 TCP Server
host = '0.0.0.0'
port = 3333
factory = protocol.ServerFactory()
factory.protocol = MyServer
reactor.listenTCP(port, factory, interface=host)
reactor.run()