import asyncio
from loguru import logger
async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    # 处理客户端连接
    logger.debug(f'客户端已连接：{client_address}')

    # 接收客户端数据并发送响应
    while True:
        data = await reader.read(1024)
        logger.debug(f'接收到客户端数据：{data.decode()}')
        if not data:
            break
        response = f'你发送的消息是：{data.decode()}'
        writer.write(response.encode())
        data = b'0123456789' * 1024 * 100
        logger.debug('send data')
        writer.write(data)
        logger.debug('send done')
        await writer.drain()
        logger.debug('write drain')

    # 关闭客户端连接
    print(f'客户端断开连接：{client_address}')
    writer.close()

async def main():
    # 创建一个 TCP 服务器
    logger.debug('start')
    server = await asyncio.start_server(handle_client, host='0.0.0.0', port=3333)

    # 启动服务器
    async with server:
        await server.serve_forever()


asyncio.run(main())
