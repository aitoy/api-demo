# 请求域名
可使用https:// 或 wss:// + 以下域名作http 或 ws接口
- api2.aitoy.fun
- ~~开发: api-dev.aitoy.fun~~
- ~~测试: api.aitoy.fun~~

# http接口文档
- https://{域名}/docs

# ws接口
- 接口 wss://{域名}
## v1 非流式
- /v1/audio2audio 发送音频返回音频
- /v1/audio2text 发送音频返回文本

## v2 流式
- /v2/audio 发送音频，返回分段文本+音频
- /v2/audio2audio 发送音频，返回分段音频
- /v2/audio2text 发叛党音频，返回分段文本

## demo 调试
- /demo/ws,发送任意数据，返回已收到数据长度

## gpt 调试
- /gpt/demo 发送文本，返回文本。调试gpt

## baidu
- /baidu/audio2text 发送音频，返回百度文本合成音频
- /baidu/text2audio 发送文本，返回百度语音识别文本

## 品恒ws接口demo
- //PHASR0000c049efbdb7e5, 品恒开发板接口，接收文本，返回chatgpt回复的文本