import requests

url = 'https://example.com/stream'
headers = {'Content-Type': 'audio/wav'}

def audio_data():
    with open('audio.wav', 'rb') as f:
        while True:
            chunk = f.read(1024)
            if not chunk:
                break
            yield chunk

response = requests.post(url, headers=headers, data=audio_data(), stream=True)

print(response.status_code)
print(response.text)