import requests

# 设置 API 地址和请求头
OPENAI_API_KEY ="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"
API_URL = "https://chatgpt-proxy.klec.top/proxy/v1/audio/transcriptions"

headers = {"Authorization": f"Bearer {OPENAI_API_KEY}"}

@router.post("/upload")
async def upload_audio(file: UploadFile):
    # 在这里处理上传的音频文件
    # 可以进行音频处理、分析、存储等操作
    # 这里仅仅返回一个响应示例
    files = {"file": (file.filename, file.content_type, "application/octet-stream")}
    data = {"model": "whisper-1","prompt":"以下是普通话的句子","language":"zh"}

    response = requests.post(API_URL, headers=headers, files=files, data=data,stream=True)
    logger.debug(response.json())

    return response.json()

