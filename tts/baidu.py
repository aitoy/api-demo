from aip import AipSpeech
from loguru import logger

APP_ID = '33570571'
API_KEY = '46CnWyv6al2GGYCakATfmYnF'
SECRET_KEY = 'hym5w8yKykty5I2NY8FffpGxeCA73o4G'


"""
options 参数列表
spd	int	选填	语速,取值0-15,默认为5中语速
pit	int	选填	音调,取值0-15,默认为5中语调
vol	int	选填	音量,基础音库取值0-9,精品音库取值0-15,默认为5中音量(取值为0时为音量最小值,并非为无声)
per(基础音库)	int	选填	度小宇=1,度小美=0,度逍遥(基础)=3,度丫丫=4
per(精品音库)	int	选填	度逍遥(精品)=5003,度小鹿=5118,度博文=106,度小童=110,度小萌=111,度米朵=103,度小娇=5
aue	int	选填	3为mp3格式(默认); 4为pcm-16k;5为pcm-8k;6为wav(内容同pcm-16k); 注意aue=4或者6是语音识别要求的格式,但是音频内容不是语音识别要求的自然人发音,所以识别效果会受影响。
"""

def tts(text,lang='zh', ctp=1,**options):
    client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
    logger.info("start")
    audio_data = client.synthesis(text, lang,ctp, options)
    logger.info("end")
    return audio_data

if __name__ == '__main__':
    options = {'vol': 5,'pit':5 }
    tts('你好',options = options)