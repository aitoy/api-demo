#!/usr/bin/env python
# https://github.com/Baidu-AIP/speech_realtime_api/blob/master/python-realtime-asr/realtime_asr.py
import asyncio
import websockets
from loguru import logger
import json,uuid

class WebSocketClient:
    def __init__(self):
        self.APPID = 33570571
        self.DEV_PID = 15372
        self.APPKEY = "46CnWyv6al2GGYCakATfmYnF"
        self.url = "wss://vop.baidu.com/realtime_asr"
        self.uri = self.url + "?sn=" + str(uuid.uuid1())
        self.client = None
        self.running = False

    async def connect(self):
        self.client = await websockets.connect(self.uri)
        logger.info('client connected')

    async def send_start_params(self):
        """
        开始参数帧
        :param websocket.WebSocket ws:
        :return:
        """
        req = {
            "type": "START",
            "data": {
                "appid": self.APPID,  # 网页上的appid
                "appkey": self.APPKEY,  # 网页上的appid对应的appkey
                "dev_pid": self.DEV_PID,  # 识别模型
                "cuid": "yourself_defined_user_id",  # 随便填不影响使用。机器的mac或者其它唯一id，百度计算UV用。
                "sample": 16000,  # 固定参数
                "format": "pcm"  # 固定参数
            }
        }
        body = json.dumps(req)
        await self.client.send(body)
        self.running = True
        logger.info("send START frame with params:" + body)

    async def send_audio(self):
        """
        发送二进制音频数据，注意每个帧之间需要有间隔时间
        :param  websocket.WebSocket ws:
        :return:
        """
        chunk_ms = 160  # 160ms的录音
        chunk_len = int(16000 * 2 / 1000 * chunk_ms)
        pcm_file = '../resource/16k-0.pcm'
        with open(pcm_file, 'rb') as f:
            pcm = f.read()

        index = 0
        total = len(pcm)
        logger.info("send_audio total={}".format(total))
        while index < total:
            end = index + chunk_len
            if end >= total:
                # 最后一个音频数据帧
                end = total
            body = pcm[index:end]
            logger.debug("try to send audio length {}, from bytes [{},{})".format(len(body), index, end))
            await self.client.send(body)
            index = end
            await asyncio.sleep(chunk_ms / 1000.0)  # ws.send 也有点耗时，这里没有计算

    async def recieve(self):
        while self.running:
            message = await self.client.recv()
            logger.info(f"receive message {message}")

        self.client.close()
    async def send_finish(self):
        """
        发送结束帧
        :param websocket.WebSocket ws:
        :return:
        """
        req = {
            "type": "FINISH"
        }
        body = json.dumps(req)
        await self.client.send(body)
        logger.info("send FINISH frame")
        self.running = False

    async def send_cancel(self):
        """
        发送取消帧
        :param websocket.WebSocket ws:
        :return:
        """
        req = {
            "type": "CANCEL"
        }
        body = json.dumps(req)
        await self.client.send(body)
        logger.info("send Cancel frame")


async def test():
    client = WebSocketClient()
    await client.connect()
    await client.send_start_params()
    await client.send_audio()
    await client.recieve()
    await client.send_finish()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())