from loguru import logger
from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse

from api import phar,demo,v1,baidu,gpt,v2,v3,whisper

app = FastAPI()
@app.get("/")
async def root(request: Request):
    return RedirectResponse(url="/docs")

app.include_router(phar.router)
app.include_router(demo.router)
app.include_router(baidu.router)
app.include_router(gpt.router)
app.include_router(v1.router)
app.include_router(v2.router)
app.include_router(v3.router)
app.include_router(whisper.router)

# 运行 FastAPI 应用
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
